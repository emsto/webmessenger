<?php
require_once '/../includes/config.php';
require_once APPLICATION_ROOT . '/includes/session.php';
require_once APPLICATION_ROOT . '/includes/message.php';
require_once APPLICATION_ROOT . '/includes/group.php';
require_once APPLICATION_ROOT . '/includes/user.php';

defined('PAGE_ID') || define('PAGE_ID', 'MESSAGES');
defined('PAGE_TITLE') || define('PAGE_TITLE', 'Messages');
defined('PAGE_DESCRIPTION') || define('PAGE_DESCRIPTION', 'List messages');

$groups = array();
foreach (group_find_all() as $group) {
    $groups[$group['id']] = $group['name'];
}

$selected_group = isset($_GET['group']) ? (int) $_GET['group'] : 0;
$selected_order = isset($_GET['order']) ? strtolower($_GET['order']) : 'asc';

include APPLICATION_ROOT . '/phtml/header.php';
?>

<div class="navbar navbar-default" role="navigation">
    <div class="navbar-collapse">
        <a class="btn btn-primary navbar-btn pull-left" href="create.php" title="New message"><span class="glyphicon glyphicon-envelope"></span> New message</a>

        <form class="navbar-form pull-right" role="search">
            <div class="form-group">
                <select class="form-control" name="group">
                    <option value="0">All</option>
                    <?php foreach ($groups as $id => $name): ?>
                        <option value="<?php echo $id ?>"<?php if ($id == $selected_group): ?> selected="selected"<?php endif; ?>><?php echo htmlentities($name) ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control" name="order">
                    <option value="asc">ASC</option>
                    <option value="desc"<?php if ('desc' == $selected_order): ?> selected="selected"<?php endif; ?>>DESC</option>
                </select>
            </div>
            <input class="btn btn-default" type="submit" value="Filter" />
        </form>
    </div>
</div>

<ul class="media-list">
    <?php foreach (message_find_all($selected_group, $selected_order) as $message): ?>
        <li class="media">
            <a class="pull-left" href="#">
                <img class="media-object" src="http://lorempixel.com/64/64/" alt="">
            </a>
            <div class="media-body">
                <h4 class="media-heading"><?php echo htmlentities($message['title']) ?><?php if (session_get_user_type() == USER_TYPE_ADMIN): ?> <small><a onclick="javascript:return confirm('Are you sure you want to delete this message?');" href="delete.php?id=<?php echo $message['id'] ?>">Delete</a></small><?php endif; ?></h4>
                <?php echo htmlentities($message['message']) ?>
                <hr />
                <p class="help-block">Posted by <span class="label label-default"><?php echo htmlentities($message['user']) ?></span> in <span class="label label-default"><?php echo htmlentities($message['group']) ?></span> group at <span class="label label-default"><?php echo $message['create_dt'] ?></span></p>
            </div>
            <hr />
        </li>
    <?php endforeach; ?>
</ul>

<?php
include APPLICATION_ROOT . '/phtml/footer.php';
