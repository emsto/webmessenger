<?php
require_once '/../includes/config.php';
require_once APPLICATION_ROOT . '/includes/session.php';
require_once APPLICATION_ROOT . '/includes/message.php';
require_once APPLICATION_ROOT . '/includes/group.php';
require_once APPLICATION_ROOT . '/includes/validate.php';

defined('PAGE_ID') || define('PAGE_ID', 'MESSAGES');
defined('PAGE_TITLE') || define('PAGE_TITLE', 'Messages');
defined('PAGE_DESCRIPTION') || define('PAGE_DESCRIPTION', 'Create new message');

$groups = array();
foreach (group_find_all() as $group) {
    $groups[$group['id']] = $group['name'];
}

$group_id = 0;

$errors = array();
if ($_POST) {

    $title = trim($_POST['title']);
    $group_id = (int) $_POST['group_id'];
    $message = trim($_POST['message']);

    if (($title_error = validate_length($title, 1, 50)) !== VALIDATION_OK) {
        $errors['title'] = $title_error;
    }
    
    if (!group_id_exists($group_id)) {
        $errors['group_id'] = 'Please, select a group from the list.';
    }

    if (($message_error = validate_length($message, 1, 250)) !== VALIDATION_OK) {
        $errors['message'] = $message_error;
    }

    if (empty($errors)) {
        $message = array(
            'group_id' => $group_id,
            'title' => $title,
            'message' => $message,
            'user_id' => session_get_user_id(),
            'create_dt' => date('Y-m-d H:i:s')
        );
        
        if (message_create($message)) {
            session_add_success_messages('Message posted successfully!');
            header('Location: list.php');
            exit;
        } else {
            session_add_error_messages('An error occured while trying to post the message.');
        }
    }
}

include APPLICATION_ROOT . '/phtml/header.php';
?>

<div class="row">
    <div class="panel panel-default col-md-6 col-md-offset-3">
        <div class="panel-body">
            <form role="form" method="post">
                <div class="form-group<?php if (isset($errors['title'])): ?> has-error<?php endif; ?>">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" maxlength="50" placeholder="Message title" value="<?php echo $_POST ? $title : '' ?>">
                    <?php if (isset($errors['title'])): ?><p class="help-block has-error"><?php echo $errors['title'] ?></p><?php endif; ?>
                </div>
                <div class="form-group<?php if (isset($errors['group_id'])): ?> has-error<?php endif; ?>">
                    <label for="title">Group</label>
                    <select class="form-control" name="group_id">
                        <option value=""></option>
                        <?php foreach ($groups as $id => $name): ?>
                            <option value="<?php echo $id ?>"<?php if ($id == $group_id): ?> selected="selected"<?php endif; ?>><?php echo htmlentities($name) ?></option>
                        <?php endforeach; ?>
                    </select>
                    <?php if (isset($errors['group_id'])): ?><p class="help-block has-error"><?php echo $errors['group_id'] ?></p><?php endif; ?>
                </div>
                <div class="form-group<?php if (isset($errors['message'])): ?> has-error<?php endif; ?>">
                    <label for="message">Message</label>
                    <textarea class="form-control" id="message" name="message" maxlength="250" placeholder="Message text"><?php echo $_POST ? $message : '' ?></textarea>
                    <?php if (isset($errors['message'])): ?><p class="help-block has-error"><?php echo $errors['message'] ?></p><?php endif; ?>
                </div>
                <button type="submit" class="btn btn-primary">Post message</button>
                or <a href="list.php">Cancel</a>
            </form>
        </div>
    </div>
</div>

<?php
include APPLICATION_ROOT . '/phtml/footer.php';
