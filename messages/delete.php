<?php

require_once '/../includes/config.php';
require_once APPLICATION_ROOT . '/includes/session.php';
require_once APPLICATION_ROOT . '/includes/message.php';
require_once APPLICATION_ROOT . '/includes/user.php';

if (session_get_user_type() !== USER_TYPE_ADMIN) {
    session_add_error_messages('You are not allowed to perform delete action.');
    header('Location: list.php');
    exit;
}

if (!isset($_GET['id'])) {
    header('Location: list.php');
    exit;
}

$message_id = (int) $_GET['id'];

if (message_delete($message_id)) {
    session_add_success_messages('Message deleted successfully.');
} else {
    session_add_error_messages('Message not found.');
}

header('Location: list.php');
exit;
