            <!-- Footer -->
            <hr />
            <p class="text-center"><strong><small><?php echo sprintf('%s (v%s)', APPLICATION_NAME, APPLICATION_VERSION) ?> &copy <?php echo date('Y'); ?> by <a href="http://telerikacademy.com" target="_blank">TelerikAcademy</a></small></strong></p>
            
        </div> <!-- /container -->

        <script src="<?php echo APPLICATION_BASE_URL ?>/assets/js/jquery.js"></script>
        <script src="<?php echo APPLICATION_BASE_URL ?>/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo APPLICATION_BASE_URL ?>/assets/js/holder.js"></script>
    </body>
</html>
