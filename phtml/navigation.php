<?php

require_once '/../includes/session.php';
require_once '/../includes/user.php';
?>

<!-- Navigation -->
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#" title="<?php echo sprintf('%s (v%s)', APPLICATION_NAME, APPLICATION_VERSION) ?>"><?php echo APPLICATION_NAME ?></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li<?php if (defined('PAGE_ID') && PAGE_ID == 'MESSAGES'): ?> class="active"<?php endif; ?>><a href="<?php echo APPLICATION_BASE_URL . '/messages/list.php' ?>">Messages</a></li>
                <?php if (session_get_user_type() == USER_TYPE_ADMIN): ?>
                    <li<?php if (defined('PAGE_ID') && PAGE_ID == 'GROUPS'): ?> class="active"<?php endif; ?>><a href="<?php echo APPLICATION_BASE_URL . '/groups/list.php' ?>">Groups</a></li>
                <?php endif; ?>
            </ul>
            <ul class="nav navbar-nav pull-right">
                <li><a>Welcome, <?php echo htmlentities(session_get_user_username()) ?>!</a></li>
                <li><a href="<?php echo APPLICATION_BASE_URL . '/logout.php' ?>">Logout</a></li>
            </ul>
        </div>
    </div>
</div>
