<?php

require_once 'config.php';
require_once 'db.php';

/**
 * 
 * @param array $data
 * @return bool
 */
function message_create(array $data)
{
    $sql = 'INSERT INTO ' . DB_TABLE_MESSAGES . '(group_id, title, message, user_id, create_dt) VALUES (?, ?, ?, ?, ?)';

    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, $sql);

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_stmt_bind_param($stmt, 'issis', $data['group_id'], $data['title'], $data['message'], $data['user_id'], $data['create_dt']);
    mysqli_stmt_execute($stmt);

    return mysqli_stmt_affected_rows($stmt) > 0;
}

/**
 * 
 * @param int $id
 * @return bool
 */
function message_delete($id)
{
    $sql = 'DELETE FROM ' . DB_TABLE_MESSAGES . ' WHERE id = ?';

    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, $sql);

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_stmt_bind_param($stmt, 'i', $id);
    mysqli_stmt_execute($stmt);

    return mysqli_stmt_affected_rows($stmt) > 0;
}

/**
 * 
 * @param int $group_id
 * @param string $order
 * @return array
 */
function message_find_all($group_id = 0, $order = 'asc')
{
    $sql = 'SELECT m.id, g.name AS `group`, m.title, m.message, u.username, m.create_dt FROM messages AS m JOIN groups AS g ON m.group_id = g.id JOIN users AS u ON m.user_id = u.id';

    $group_id = (int) $group_id;
    if ($group_id > 0) {
        $sql .= ' WHERE g.id = ' . $group_id;
    }

    $order = strtolower($order);
    if (!in_array($order, array('asc', 'desc'))) {
        $order = 'asc';
    }

    $sql .= ' ORDER BY m.create_dt ' . $order;

    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, $sql);

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $id, $group, $title, $message, $username, $create_dt);

    $messages = array();
    while (mysqli_stmt_fetch($stmt)) {
        $messages[] = array(
            'id' => $id,
            'group' => $group,
            'title' => $title,
            'message' => $message,
            'user' => $username,
            'create_dt' => $create_dt
        );
    }

    return $messages;
}
