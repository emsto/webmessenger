<?php

define('APPLICATION_NAME', 'WebMessenger');
define('APPLICATION_VERSION', '0.0.2');

$document_root = str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']);
$dirname = str_replace('\\', '/', dirname(__FILE__));
$baseurl = substr($dirname, strlen($document_root));
$baseurl = substr($baseurl, 0, strrpos($baseurl, '/'));

define('APPLICATION_BASE_URL', $baseurl);
define('APPLICATION_ROOT', dirname(dirname(__FILE__)));

define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'web_messages');
define('DB_PASSWORD', 'e3e36GVXuRBR5QsN');
define('DB_DATABASE', 'web_messages');

define('DB_TABLE_USERS', 'users');
define('DB_TABLE_GROUPS', 'groups');
define('DB_TABLE_MESSAGES', 'messages');
