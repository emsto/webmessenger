<?php

require_once 'config.php';

$mysqli_link = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

function mysqli_get_link()
{
    global $mysqli_link;
    return $mysqli_link;
}
