<?php

require_once 'config.php';
require_once 'db.php';

/**
 * 
 * @param array $data
 * @return bool
 */
function group_create(array $data)
{
    $sql = 'INSERT INTO ' . DB_TABLE_GROUPS . '(name) VALUES (?)';

    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, $sql);

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_stmt_bind_param($stmt, 's', $data['name']);
    mysqli_stmt_execute($stmt);

    return mysqli_stmt_affected_rows($stmt) > 0;
}

/**
 * 
 * @param int $id
 * @return boolean
 */
function group_id_exists($id)
{
    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, 'SELECT id FROM ' . DB_TABLE_GROUPS . ' WHERE id = ?');

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_stmt_bind_param($stmt, 'i', $id);
    mysqli_stmt_execute($stmt);
    while (mysqli_stmt_fetch($stmt)) {
        return true;
    }

    return false;
}

/**
 * 
 * @param int $id
 * @return boolean
 */
function group_name_exists($name)
{
    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, 'SELECT id FROM ' . DB_TABLE_GROUPS . ' WHERE LOWER(name) = ?');

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    $name = strtolower($name);
    mysqli_stmt_bind_param($stmt, 's', $name);
    mysqli_stmt_execute($stmt);
    while (mysqli_stmt_fetch($stmt)) {
        return true;
    }

    return false;
}

/**
 * 
 * @return array
 */
function group_find_all()
{
    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, 'SELECT id, name FROM ' . DB_TABLE_GROUPS . ' ORDER BY name ASC');

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $group_id, $group_name);

    $groups = array();
    while (mysqli_stmt_fetch($stmt)) {
        $groups[] = array('id' => $group_id, 'name' => $group_name);
    }

    return $groups;
}
