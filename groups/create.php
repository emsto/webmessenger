<?php
require_once '/../includes/config.php';
require_once APPLICATION_ROOT . '/includes/session.php';
require_once APPLICATION_ROOT . '/includes/message.php';
require_once APPLICATION_ROOT . '/includes/group.php';
require_once APPLICATION_ROOT . '/includes/validate.php';
require_once APPLICATION_ROOT . '/includes/user.php';

if (session_get_user_type() != USER_TYPE_ADMIN) {
    session_add_error_messages('You are not allowed to access requested resource.');
    header('Location: ' . APPLICATION_BASE_URL . '/index.php');
    exit;
}

defined('PAGE_ID') || define('PAGE_ID', 'GROUPS');
defined('PAGE_TITLE') || define('PAGE_TITLE', 'Groups');
defined('PAGE_DESCRIPTION') || define('PAGE_DESCRIPTION', 'Create new group');

$errors = array();
if ($_POST) {

    $name = trim($_POST['name']);

    if (($name_error = validate_length($name, 1, 25)) !== VALIDATION_OK) {
        $errors['name'] = $name_error;
    } elseif (group_name_exists($name)) {
        $errors['name'] = 'Group with this name already exists';
    }

    if (empty($errors)) {
        $group = array(
            'name' => $name,
        );

        if (group_create($group)) {
            session_add_success_messages('Group created successfully!');
            header('Location: list.php');
            exit;
        } else {
            session_add_error_messages('An error occured while trying to create the group.');
        }
    }
}

include APPLICATION_ROOT . '/phtml/header.php';
?>

<div class="row">
    <div class="panel panel-default col-md-6 col-md-offset-3">
        <div class="panel-body">
            <form role="form" method="post">
                <div class="form-group<?php if (isset($errors['name'])): ?> has-error<?php endif; ?>">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name" maxlength="25" placeholder="Group name" value="<?php echo $_POST ? $name : '' ?>">
                    <?php if (isset($errors['name'])): ?><p class="help-block has-error"><?php echo $errors['name'] ?></p><?php endif; ?>
                </div>
                <button type="submit" class="btn btn-primary">Create group</button>
                or <a href="list.php">Cancel</a>
            </form>
        </div>
    </div>
</div>

<?php
include APPLICATION_ROOT . '/phtml/footer.php';
