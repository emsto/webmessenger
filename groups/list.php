<?php
require_once '/../includes/config.php';
require_once APPLICATION_ROOT . '/includes/session.php';
require_once APPLICATION_ROOT . '/includes/group.php';
require_once APPLICATION_ROOT . '/includes/user.php';

if (session_get_user_type() != USER_TYPE_ADMIN) {
    session_add_error_messages('You are not allowed to access requested resource.');
    header('Location: ' . APPLICATION_BASE_URL . '/index.php');
    exit;
}

defined('PAGE_ID') || define('PAGE_ID', 'GROUPS');
defined('PAGE_TITLE') || define('PAGE_TITLE', 'Groups');
defined('PAGE_DESCRIPTION') || define('PAGE_DESCRIPTION', 'List message groups');

include APPLICATION_ROOT . '/phtml/header.php';
?>

<div class="navbar navbar-default" role="navigation">
    <div class="navbar-collapse">
        <a class="btn btn-primary navbar-btn pull-left" href="create.php" title="New message"><span class="glyphicon glyphicon-plus-sign"></span> New group</a>
    </div>
</div>

<table class="table table-condensed table-hover table-responsive">
    <thead>
        <tr>
            <th>Group name</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach (group_find_all() as $group): ?>
            <tr>
                <td><?php echo htmlentities($group['name']); ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php
include APPLICATION_ROOT . '/phtml/footer.php';
