<?php

require_once 'includes/config.php';
require_once 'includes/session.php';

session_destroy();

header('Location: ' . APPLICATION_BASE_URL . '/login.php');
